## About the challenge

What we expect to learn from you with this challenge:

- Your work style.
- How you think and solve problems.
- How you communicate.

What we expect that you learn from us:

- How we work as a team.
- Have a close look at some of the problems we face daily.

## Next steps

1. Fork this repository to your personal account.
2. Follow the instructions in the [`challenge-description`](/challenge-description.md) file.
3. Solve the challenge in the best way you can.
4. Send us a PR with your solution.

## Considerations

- You are free to choose any tools or libraries. But make choices that suits your needs. The challenge is to evaluate your skills.
- Despite not having a time limit, we recommend that you don't spend more than **12 hours** working on this challenge.
- Try to write the best code you possibly can. It will make our life easier when evaluating your solution.