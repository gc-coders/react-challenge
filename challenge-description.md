# Goodcompany react challenge

The objective of this challenge is to build a catalog page for some gifts on Goodcompany.

## Rules

- The page can't be static. It needs some logic to show all gifts dynamically.
- All gifts from `gifts.json` must be shown.
- You need to use Webpack to bundle your files. 
- The code must be written in typescript. 
- Don't forget to add instructions on how to run the project.
- Do not ask your friends to solve the challenge for you. You'll need to explain your choices in person.

## Requirements

- The app must be responsive. Use a **mobile-first** approach.
- For each item on the list, the following information must be present on the page:
    - Image
    - Name
    - Price
    - Description
    - Charity Logo
- It must be possible to add gifts to a cart.
- It must be possible to view the cart with the items you've added (name, image, price, quantity) and the grand total.
- The cart should be persisted between reloads (redux).

## Bonus points

- Easy as pie? So add more functionality to cart allowing users to add, remove and change quantity of cart items.
- Add Server Side Rendering to the app. 
- Want more challenges? Add a filter to show gifts by `gifttype` or a search box.
- Add sort by `_score` sorting.
- We like clean and testable code, right? Test your application and components, it will be a big bonus point.

## What we will evaluate

- Your code will be evaluated by: semantics, structure, legibility, size, among other factors.
- The `git` history will be avaluated.
- Our stack here is Typescript, React, Redux, Next JS, SCSS and Webpack so using the same stack will make a difference.
- We're looking for a front-end developer who knows how to build layouts and knows a lot about **Typescript**. Use this as an opportunity to show us how good you are on both areas.
- Do not forget to document the process needed to run and build your app. Or else how are we going to evaluate your work if we can't init the project on our machines?
